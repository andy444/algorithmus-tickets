def process_tickets(tickets):
  # the first operation is to sort tickets, so destination point of one ticket is start point to the next ticket
  sorted_tickets = tickets.copy()
  for ticket in tickets:
    # remove current ticket from the second list to avoid duplicates
    sorted_tickets.remove(ticket)
    for i, element in enumerate(sorted_tickets):
      # compare destination on the first list with start point on the second list.
      if ticket[1] == element[0]:
        sorted_tickets.insert(i, ticket)
        break
    # when there is no start point, then it's obvious the last destination. Hence it should be placed in the end.
    sorted_tickets.append(ticket)

  # after the tickets are sorted, second part is to make a list of all countries and remove duplicates with preserving order
  countries_list = []
  for ticket in sorted_tickets:
    for country in ticket:
      countries_list.append(country)
  # remove duplicates
  countries_list = list(dict.fromkeys(countries_list))
  # convert the list into comma-separated string
  return ', '.join(countries_list)

tickets = [["JPN", "PHL"], ["BRA", "UAE"], ["USA", "BRA"], ["UAE", "JPN"]]
result = process_tickets(tickets)
print(result)